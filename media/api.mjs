
import path from 'path'

import HTTPUtil from 'zunzun/flyutil/http.mjs'

import FFMPEG from './ffmpeg.mjs';


export default class MediaAPI {
  constructor(flycms) {
    const flyadmin = flycms.flyadmin;

    const get_regexp = /^\/flycms-media-get\/([^?\/]+)(\?.*)?/;
    flycms.http.get(get_regexp, ...flyadmin.default_middlewares, flyadmin.authorize_mwfn, async (req, res) => {
      try {
        const params = req.flyauth.params;
        const album_name = req.regex_match[0];
        console.log();
        const album = flycms.albums[album_name];

        const media_files =  await album.get_page(params.offset, params.limit, params.sort, params.desc);

        console.log(media_files);

        res.writeHead(200, { 'Content-Type': 'application/json' });
        res.end(JSON.stringify(media_files));

      } catch (e) {
        console.error(e.stack);
      }

    });

    FFMPEG.set_callback(async() => {
      try {
        await flycms.flyweb.route_mg.update_context_group("/");
        await flycms.flyweb.route_mg.update_context_group("/flycms");
      } catch (e) {
        console.error(e.stack);
      }
    });

    const upload_regexp = /^\/flycms-media-upload\/([^?\/]+)$/;
    flycms.http.post(upload_regexp, ...flyadmin.default_middlewares, flyadmin.authorize_mwfn, async (req, res) => {
      try {
        console.log("UPLOAD MEDIA");
        const album_name = req.url.match(upload_regexp)[1];
        const album = flycms.albums[album_name];
        const uploaded_files = await HTTPUtil.multipart(req, (filename) => {
          return path.resolve(album.path, filename);
        });
        for (let upfile of uploaded_files) {
          FFMPEG.convert(upfile);
        }

        await flycms.flyweb.route_mg.update_context_group("/");
        await flycms.flyweb.route_mg.update_context_group("/flycms");

        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.end('OK');

      } catch (e) {
        console.error(e.stack);
      }

    });

    const delete_regexp = /^\/flycms-media-remove\/([^?\/]+)\/([^?\/]+)$/;
    flycms.http.delete(delete_regexp, ...flyadmin.default_middlewares, flyadmin.authorize_mwfn, async (req, res) => {
      try {
        console.log("REMOVE MEDIA");
        const url_match = req.url.match(delete_regexp);
        const album_name = url_match[1];
        const album = flycms.albums[album_name];

        const media_name = url_match[2];
        album.delete(media_name);


        await flycms.flyweb.route_mg.update_context_group("/");
        await flycms.flyweb.route_mg.update_context_group("/flycms");

        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.end('OK');

      } catch (e) {
        console.error(e.stack);
      }
    });
  }
}
