
import fs from 'fs'
import path from 'path'

import ExecUtil from "zunzun/flyutil/exec.mjs";

let conversion_queue = [];
let converting = false;
let converted_cb = undefined;
const convert = async () => {
  if (!converting) {
    converting = true;
    for (let cc of conversion_queue) {
      try {
        await ExecUtil.command_async(cc)
      } catch (e) {
        console.error(e.stack);
      }
    }
    conversion_queue = [];
    converted_cb();
    converting = false;
  }
}

export default class FFMPEG {

  static async convert(input_path) {
    try {
      if (input_path.match(/(ogv|mp4|webm)$/)) {
        const command = `ffmpeg -threads 4 -i "${input_path}"`;
  //      const video_optimizations =  `-preset ultrafast -crf 28 -vf "scale=1920:1080:force_original_aspect_ratio=decrease"`;
        const video_optimizations = `-b:v 1M -vf "scale='if(gt(iw,1280),1280,iw)':'if(gt(ih,720),720,ih)'" -b:a 128k`;

        const out_mp4 = input_path.replace(/(ogv|mp4|webm)$/, "mp4");
        const mp4_params = ` -c:v libx264 ${video_optimizations} -c:a aac -strict experimental -y "${out_mp4}"`;
        if (!input_path.match(/mp4$/)) conversion_queue.push(command + mp4_params);

        const out_ogv = input_path.replace(/(ogv|mp4|webm)$/, "ogv");
        const ogv_params = ` -c:v libtheora -q:v 7 ${video_optimizations} -c:a libvorbis -q:a 4 -y "${out_ogv}"`;
        if (!input_path.match(/ogv$/)) conversion_queue.push(command + ogv_params);

        const out_webm = input_path.replace(/(ogv|mp4|webm)$/, "webm");
        const webm_params = ` -c:v libvpx-vp9 ${video_optimizations} -c:a libopus -speed 4 -y "${out_webm}"`;
        if (!input_path.match(/webm$/)) conversion_queue.push(command + webm_params);

        console.log("call convert");
        setTimeout(() => {
          convert();
        }, 0);
        console.log("convert called");
      } else if (input_path.match(/(jpg|jpeg|png)$/)) {
        let cmd = `ffmpeg -y -i ${input_path}`;
        cmd += ` -vf "scale='if(gt(iw,ih),200,-1)':'if(gt(iw,ih),-1,200)'"`;

        const file_name = input_path.match(/[^/]+$/)[0];
        const album_name = input_path.match(/([^/]+)\/[^/]+$/)[1];
        const thumb_path = path.join(
          input_path.replace(/\/[^/]+$/, ""),
          "../../thumbnails"
        );
        if (!fs.existsSync(thumb_path)) fs.mkdirSync(thumb_path);
        const thumb_album_path = path.join(thumb_path, album_name);
        if (!fs.existsSync(thumb_album_path)) fs.mkdirSync(thumb_album_path);
        const output_path = path.join(thumb_album_path, file_name);
        cmd += ` ${output_path}`;

        conversion_queue.push(cmd)

        console.log("call convert");
        setTimeout(() => {
          convert();
        }, 0);
      }
    } catch (e) {
      console.error(e.stack);
    }
  }

  static set_callback(cb) {
    converted_cb = cb;
  }
}
