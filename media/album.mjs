

import fs from 'fs'
import path from 'path'

import {fileTypeFromStream} from 'file-type'

export default class MediaAlbum {
  constructor(name, api_name, flycms) {
    const _this = this;
    this.name = name,
    this.path = path.resolve(flycms.content_mg.albums_path, api_name);

    this.flycms = flycms;

  }

  count() {
    return fs.readdirSync(this.path).length;
  }

  async get_page(offset, limit, sort, desc) {
    try {
      console.log("GET ALBUM", this.path);
      const media_files = fs.readdirSync(this.path);
      if (desc) {
        media_files.sort((a, b) => -a.localeCompare(b));
      } else {
        media_files.sort((a, b) => a.localeCompare(b));
      }

      const valid_files = [];
      for (let i = 0; i < media_files.length; i++) {
        if (!media_files[i].match(/(ogv|webm)$/)) {
          const mfile_path = path.resolve(this.path, media_files[i]);
          const stat = fs.statSync(mfile_path);
          valid_files.push({
            name: media_files[i],
            modified: stat.mtime.getTime()
          });
        }
      }


      if (sort == "date") {
        valid_files.sort((a, b) => {
          if (desc) {
            return b.modified - a.modified;
          } else {
            return a.modified - b.modified;
          }
        });
      }

      let end_offset = offset + parseInt(limit);
      if (end_offset > valid_files.length) end_offset = valid_files.length;

      const page_files = [];
      for (let i = offset; i < end_offset; i++) {
        const mfile = valid_files[i].name;
        const mfile_path = path.resolve(this.path, mfile);
        const stream = fs.createReadStream(mfile_path);
        const file_type = await fileTypeFromStream(stream);

        page_files.push({
          file: mfile,
          mime: file_type ? file_type.mime.split("/")[0] : "unknown"
        });
      }


      return page_files;
    } catch (e) {
      console.error(e.stack);
    }
  }

  async delete(media_name) {
    try {
      const media_path = path.resolve(this.path, media_name);

      if (fs.existsSync(media_path)) {
        fs.unlinkSync(media_path);
      }
    } catch (e) {
      console.error(e.stack);
    }
  }
}
