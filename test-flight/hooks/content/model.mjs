import fs from 'fs';

import crypto from 'crypto'

export default class ContentModel {
  static async construct(sm) {
    const _this = new ContentModel();
    _this.flycms = sm.get_service_instance("flycms");
    _this.content_mg = _this.flycms.content_mg;
    return _this;
  }

  constructor() {
  }

  async delete(model_name) {
    const model = this.flycms.models[model_name];
    if (!model) throw new Error(`Model "${model_name}" does not exist!`);
    await model.table.delete("ALL");
  }

  async insert_random(model_name, cfg) {
    const model = this.flycms.models[model_name];
    if (!model) throw new Error(`Model "${model_name}" does not exist!`);

    const columns = [];
    const expressions = [];
    const values = [];
    const obj = {};
    let iexpr = 1;
    for (let column of model.columns) {
      columns.push(column.name);
      expressions.push("$"+iexpr);
      let val = undefined;
      if (
        (
          column.type == "int" || 
          column.type == "float" || 
          column.type == "double"
        )
      ) {
        if (cfg && cfg.values && typeof cfg.values.number == "number") {
          val = cfg.values.number
        } else {
          val = column.type == "int" ?
            Math.round(Math.random()*9999999999-9999999999/2) :
            Math.random()*9999999999-9999999999/2;
        }
      } else if (column.type == "text") {

        if (cfg && cfg.values && typeof cfg.values.text == "string") {
          val = `'`+cfg.values.text+`'`
        } else {
          val = `'`+crypto.randomBytes(32).toString("base64")+`'`;
        }
      }

      if (column.pg_type && column.pg_type.endsWith("[]")) val = `ARRAY[`+val+`]`;

      values.push(val);
      obj[column.name] = val;
      iexpr++;
    }

    const columns_str = columns.join(", ");
    const values_str = values.join(", ");

    const qstr = `INSERT INTO ${model.table.name} (${columns_str}) VALUES (${values_str}) RETURNING ID;`
    obj.id = (await model.table.query(qstr)).rows[0].id;
    return obj;
  }

}
