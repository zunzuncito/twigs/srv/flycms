
import FlyTest from "zunzun/test-flight/index.mjs"
const test = FlyTest.Test.test;
const test_async = FlyTest.Test.test_async;
const fail = FlyTest.fail;

const Skip = FlyTest.Skip;
const Sequence = FlyTest.Sequence;
const Assert = FlyTest.Assert;

export default class EncryprionAPITest {
  static async open_endpoints_public(cfg, tool) {
    try {
      const seq = new Sequence();
      seq.test('GET encryption', () => {
        throw new Skip("Not implemented!");
      });
 
      seq.test('POST encryption', () => {
        throw new Skip("Not implemented!");
      });
 
      seq.test('PUT encryption', () => {
        throw new Skip("Not implemented!");
      });
 
      seq.test('DELETE encryption', () => {
        throw new Skip("Not implemented!");
      });

      seq.test('COUNT encryption', () => {
        throw new Skip("Not implemented!");
      });

      seq.throw();
    } catch (e) {
      console.error(e.stack);
    }
  }
}
