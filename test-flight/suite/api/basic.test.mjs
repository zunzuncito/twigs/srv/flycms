
import FlyTest from "zunzun/test-flight/index.mjs"
const test = FlyTest.Test.test;
const test_async = FlyTest.Test.test_async;
const fail = FlyTest.fail;

const Skip = FlyTest.Skip;
const Sequence = FlyTest.Sequence;
const Assert = FlyTest.Assert;

export default class BasicContentApiTests {
  constructor(cfg, tools) {
    this.cfg = cfg;
    this.tools = tools;

    for (let srv of cfg.env.services) {
      if (srv.name == "flyweb") {
        this.flyweb_cfg = srv.config
        break;
      }
    }
  }

  static async construct(cfg, tools) {
    try {
      const _this = new BasicContentApiTests(cfg, tools);
      _this.model_hook = await tools.hooks["srv/flycms"]("ContentModel");
      return _this;
    } catch (e) {
      console.error(e.stack);
    }
  }

  async count() {

    const _this = this;
    const seq = new Sequence();


    async function expect_count(msg, expected) {
      await seq.test_async(msg, async () => {
        const count_url = `http://${_this.flyweb_cfg.host}:${_this.flyweb_cfg.port}/flycms-api/open_model/count`;
        console.log(`GET > ${count_url}`);
        let res = await fetch(count_url);
        Assert.equal(res.status, 200, `Invalid response code: ${res.status}`);
        const res_number = await res.json();
        Assert.number(res_number, "Invalid response format!");
        Assert.equal(res_number, expected, `Invalid count: ${res_number}, expected: ${expected}`);
      });
    }



    await this.model_hook.cmd("call", "delete", "open_model");
    await expect_count('COUNT empty table', 0);

    for (let i = 0; i < 5; i++) {
      await this.model_hook.cmd("call", "insert_random", "open_model");
    }
    await expect_count('COUNT sligtly filled table', 5);

    seq.throw();
  }

  async get() {
    const flyweb_cfg = this.flyweb_cfg;

    const _this = this;

    await this.model_hook.cmd("call", "delete", "open_model");

    for (let i = 0; i < 51; i++) {
      await this.model_hook.cmd("call", "insert_random", "open_model");
    }

    const seq = new Sequence();
    await seq.test_async('GET all items limiting', async () => {
      const all_url = `http://${_this.flyweb_cfg.host}:${_this.flyweb_cfg.port}/flycms-api/open_model`;
      console.log(`GET > ${all_url}`);
      const res = await fetch(all_url);
      Assert.equal(res.status, 200, `Invalid response code: ${res.status}`);
      const res_json = await res.json();
      Assert.array(res_json, "Invalid response format!");
      Assert.gt(res_json.length, 0, "Responded with an empty array. Length: 0");
      Assert.equal(res_json.length, 50, "Responded with an empty larger then default limit: 50");
    });

    await seq.test_async('GET items with pagination', async () => {
      const paged_url = `http://${_this.flyweb_cfg.host}:${_this.flyweb_cfg.port}/flycms-api/open_model?offset=10&limit=10`;
      console.log(`GET > ${paged_url}`);
      const res = await fetch(paged_url);
      Assert.equal(res.status, 200, `Invalid response code: ${res.status}`);
      const res_json = await res.json();
      Assert.array(res_json, "Invalid response format!");
      Assert.gt(res_json.length, 0, "Responded with an empty array. Length: 0");
      Assert.equal(res_json.length, 10, "Responded with an empty larger then specified limit: 10");
    });

    let random_row = await this.model_hook.cmd("call", "insert_random", "open_model");

    await seq.test_async('GET content item by id', async () => {
      const by_id_url = `http://${_this.flyweb_cfg.host}:${_this.flyweb_cfg.port}/flycms-api/open_model/${random_row.id}`;
      console.log(`GET > ${by_id_url}`);
      const res = await fetch(by_id_url);
      Assert.equal(res.status, 200, `Invalid response code: ${res.status}`);
      const res_json = await res.json();
      Assert.array(res_json, "Invalid response format!");
      Assert.gt(res_json.length, 0, "Responded with an empty array. Length: 0");
    });

    await this.model_hook.cmd("call", "delete", "open_model");
    let price_row = await this.model_hook.cmd("call", "insert_random", [
      "open_model",
      {
        values: {
          number: 0
        }
      }
    ]);
    await this.model_hook.cmd("call", "insert_random", [
      "open_model",
      {
        values: {
          number: 1
        }
      }
    ]);

    await seq.test_async('GET content using a condition', async () => {
      const where_condition = encodeURIComponent(
        `price<0.5`
      );
      const conditional_url = `http://${_this.flyweb_cfg.host}:${_this.flyweb_cfg.port}/flycms-api/open_model?where=${where_condition}`;
      console.log(`GET > ${conditional_url}`);
      const res = await fetch(conditional_url);
      Assert.equal(res.status, 200, `Invalid response code: ${res.status}`);
      const res_json = await res.json();
      Assert.array(res_json, "Invalid response format!");
      Assert.equal(
        res_json.length, 1,
        `Responded with an array that has invalid length: ${res_json.length}`
      );
    });


    await this.model_hook.cmd("call", "delete", "open_model");

    let vals_texts = [
      [2, "test_value"], //true
      [1, "another_value"], //true
      [0.4, "not_known"], //true
      [0.01, "another_value"], //false
      [1, "not_known"], //false
    ]

    let price_extra_rows = []

    for (let val_text of vals_texts) {
      await this.model_hook.cmd("call", "insert_random", [
        "open_model",
        {
          values: {
            number: val_text[0],
            text: val_text[1]
          }
        }
      ]);
    }


    await seq.test_async('GET content using multiple conditions', async () => {
      const where_condition = encodeURIComponent(
        `((price<0.5|item='another_value')&price>0.1)|item='test_value'`
      );
      const multi_condition_url = `http://${_this.flyweb_cfg.host}:${_this.flyweb_cfg.port}/flycms-api/open_model?where=${where_condition}`;
      console.log(`GET > ${multi_condition_url}`);
      const res = await fetch(multi_condition_url);
      Assert.equal(res.status, 200, `Invalid response code: ${res.status}`);
      const res_json = await res.json();
      Assert.array(res_json, "Invalid response format!");
      Assert.equal(
        res_json.length, 3,
        `Responded with an array that has invalid length: ${res_json.length}, expected: 3`
      );
    });

    seq.throw();
  }

  async post() {
    const _this = this;
    const seq = new Sequence();
    await seq.test_async('POST request', async () => {
      const post_url = `http://${_this.flyweb_cfg.host}:${_this.flyweb_cfg.port}/flycms-api/open_model`;
      const post_body = "item=Thing&description=Good Thing&options=one,two,three";
      console.log(`POST > ${post_url} + ${post_body}`);
      const res = await fetch(post_url, {
        method: "POST",
        body: encodeURI(post_body),
      });
      Assert.equal(res.status, 200, `Invalid response code: ${res.status}`);
      const res_json = await res.json();
      Assert.object(res_json, "Invalid response format!");
      Assert.true(typeof res_json.id !== "undefined", "Response object doesn't have id property!");

    });

    await seq.test_async('POST multiple', async () => {
      throw new Skip("Not implemented!");
    });

    seq.throw();

  }

  async put() {

    const _this = this;

    let random_row = await this.model_hook.cmd("call", "insert_random", "open_model");

    const seq = new Sequence();
    await seq.test_async('PUT request', async () => {
      const put_url = `http://${_this.flyweb_cfg.host}:${_this.flyweb_cfg.port}/flycms-api/open_model/${random_row.id}`;
      const put_body = "item=Changed&description=Now Better!&options=four, five";
      console.log(`PUT > ${put_url} + ${put_body}`);
      const res = await fetch(put_url, {
        method: "PUT",
        body: encodeURI(put_body),
      });
      Assert.equal(res.status, 200, `Invalid response code: ${res.status}`);
      const res_json = await res.json();
      Assert.object(res_json, "Invalid response format!");
      Assert.true(typeof res_json.id !== "undefined", "Response object doesn't have id property!");
    });


    await seq.test_async('PUT multiple', async () => {
      throw new Skip("Not implemented!");
    });


    seq.throw();
  }

  async delete() {

    const _this = this;
    let random_row = await this.model_hook.cmd("call", "insert_random", "open_model");


    const seq = new Sequence();
    await seq.test_async('DELETE one item by id', async () => {
      const del_url = `http://${_this.flyweb_cfg.host}:${_this.flyweb_cfg.port}/flycms-api/open_model/${random_row.id}`;

      console.log(`DELETE > ${del_url}`);
      const res = await fetch(del_url, {
        method: "DELETE"
      });

      Assert.equal(res.status, 200, `Invalid response code: ${res.status}`);
      const res_text = await res.text();
      Assert.string(res_text, "Invalid response format!");
      Assert.equal(res_text, "OK", `Response text in not "OK"`);

    });

    await seq.test_async('DELETE where condition', async () => {
      throw new Skip("Not implemented!");
    });


    await seq.test_async('DELETE all', async () => {
      throw new Skip("Not implemented!");
    });

    seq.throw();
  }

  async destroy() {
    // TODO
  }
}
