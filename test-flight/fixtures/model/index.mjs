
import path from 'path'

import crypto from 'crypto'

import LibFlyCMS from 'zunzun/flycms/index.mjs'
const ContentManager = LibFlyCMS.ContentManager;

export default class ModelFixture {
  constructor(cfg, tools, aura, pg) {
    this.cfg = cfg;
    this.tools = tools;

    this.aura = aura;
    this.pg = pg;

    const flycms_path = path.resolve(cfg.twig.cwd, cfg.twig.path);
    this.content_mg = new ContentManager(flycms_path);
    this.models = this.content_mg.models;
  }

  static async construct(cfg, tools) {
    const aura = await tools.mocks["lib/pg-aura"].Aura();
    const pg = await tools.fixtures["lib/pg-aura"].Pg();

    const _this = new ModelFixture(cfg, tools, aura, pg);

    for (let model in _this.content_mg.models) {
      await _this.content_mg.models[model].construct(aura);
    }

    return _this;
  }

  async get_model(api_name) {
    try {
      return this.models[api_name];
    } catch (e) {
      console.error(e.stack);
    }
  }

  async insert_random(model, cfg) {
    try {
      const columns = [];
      const expressions = [];
      const values = [];
      const obj = {};
      let iexpr = 1;
      for (let column of model.columns) {
        columns.push(column.name);
        expressions.push("$"+iexpr);
        let val = undefined;
        if (
          (
            column.type == "int" || 
            column.type == "float" || 
            column.type == "double"
          )
        ) {
          if (cfg && cfg.values && typeof cfg.values.number == "number") {
            val = cfg.values.number
          } else {
            val = column.type == "int" ?
              Math.round(Math.random()*9999999999-9999999999/2) :
              Math.random()*9999999999-9999999999/2;
          }
        } else if (column.type == "text") {

          if (cfg && cfg.values && typeof cfg.values.text == "string") {
            val = `'`+cfg.values.text+`'`
          } else {
            val = `'`+crypto.randomBytes(32).toString("base64")+`'`;
          }
        }

        if (column.pg_type && column.pg_type.endsWith("[]")) val = `ARRAY[`+val+`]`;

        values.push(val);
        obj[column.name] = val;
        iexpr++;
      }

      const columns_str = columns.join(", ");
      const values_str = values.join(", ");

      const qstr = `INSERT INTO ${model.table.name} (${columns_str}) VALUES (${values_str}) RETURNING ID;`
      obj.id = (await this.pg.client.query(qstr)).rows[0].id;
      return obj;
    } catch (e) {
      console.error(e.stack);
    }
  }

  async teardown() {
    try {
      await this.pg.teardown();
    } catch (e) {
      console.error(e.stack);
    }
  }
}
