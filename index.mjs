
import fs from 'fs'
import path from 'path'
import { fileURLToPath } from 'url';
const __dirname = path.dirname(fileURLToPath(import.meta.url));


import YAML from 'yaml'

import LibFlyAuth from 'zunzun/authing/index.mjs'

import FSUtil from 'zunzun/flyutil/fs.mjs'
import HTTPUtil from 'zunzun/flyutil/http.mjs'

import LibFlyCMS from 'zunzun/flycms/index.mjs'
const ContentManager = LibFlyCMS.ContentManager;

import MediaAlbum from "./media/album.mjs";

import MediaAPI from './media/api.mjs'
import AdminAPI from './content/admin/api.mjs'
import PublicAPI from './content/public/api.mjs'



export default class FlyCMS {
  constructor(sm, cfg, flyweb, flyadmin, flyauth) {
    this.sm = sm;
    this.flyweb = flyweb;
    this.http = flyweb.http;
    this.flyadmin = flyadmin;
    this.flyauth = flyauth;
    this.flyauth.flycms = this;

    const flycms_path = path.resolve(process.cwd(), cfg.path);
    const srv_path = path.join(process.cwd(), 'twigs/srv');
    const srv_flycms_paths = [];
    const srv_dirs = fs.readdirSync(srv_path);
    for (let srv_dir of srv_dirs) {
      const srv_flycms_path = path.join(srv_path, srv_dir, 'flycms');
      if (fs.existsSync(srv_flycms_path)) srv_flycms_paths.push(srv_flycms_path);
    }

    this.content_mg = new ContentManager(flycms_path, srv_flycms_paths, {
      available_languages: flyweb.cfg.available_languages
    });

    this.albums_path = this.content_mg.albums_path;

    this.models = this.content_mg.models;
    this.albums = this.content_mg.albums;

    const media_dir = path.resolve(__dirname, "pages/media");

    this.media_items_path = path.resolve(__dirname, "pages/media/items");
    if (fs.existsSync(this.content_mg.albums_path)) {
      FSUtil.symlink(this.content_mg.albums_path, this.media_items_path);
    }

    setInterval(() => {
      if (!fs.existsSync(this.media_items_path) && fs.existsSync(media_dir)) {
        FSUtil.symlink(this.content_mg.albums_path, this.media_items_path);
      }
    }, 100);
  }



  static async start(sm, cfg) {
    try {
      const pg = sm.get_service_instance("pg");
      const flyweb = sm.get_service_instance("flyweb");
      const flyauth = sm.get_service_instance("authing");


      const cms_web_path = cfg.route_path || "/flycms"

      const ctx_group = await flyweb.route_mg.serve_context_group(cms_web_path, path.resolve(__dirname, "pages"));
      const flyadmin = await LibFlyAuth.construct(pg, ctx_group, cfg);



      const album_dir = path.resolve(cfg.path);
      const _this = new FlyCMS(sm, cfg, flyweb, flyadmin, flyauth);
      
      for (let model in _this.content_mg.models) {
        await _this.content_mg.models[model].construct(pg);

        if (_this.content_mg.models[model].has_media) {
          _this.content_mg.albums[model] = new MediaAlbum(_this.content_mg.models[model].name, _this.content_mg.models[model].api_name, _this);
        }
      }


      new MediaAPI(_this);
      new AdminAPI(_this);
      new PublicAPI(_this);

      await flyweb.route_mg.update_context_group("/flycms");

      if (fs.existsSync(_this.content_mg.api_hooks_path)) await _this.content_mg.load_api_hooks();
 

      return _this;
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  get_model(model_name) {
    if (!this.content_mg.models[model_name]) {
      throw new Error(`Model '${model_name}' does not exist!`);
    } else {
      return this.content_mg.models[model_name];
    }
  }

/*
  async add_page(title, dir_path) {
    await this.web_dir.add_context(title, dir_path);
    this.web_dir.g_tpl_ctx.header_menu.push({
      text: title,
      href: `${this.web_path}/${title}/index.html`
    });

  }*/
}
