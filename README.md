

# `flycms` - Security oriented headless content management system


## Model configuration

By default `flycms` looks for model files in `flycms/models` within your project directory. Model files are written in `yaml` format. For example `faq.yaml`:

```
name: Frequently Asked Questions
api_name: faq
columns:
  question:
    type: text
    unique: true
    required: true
  answer: 
    type: text
    required: true

```

### Model properties
 - `string` `name` - display name
 - `string` `api_name` - name used in the API
 - `string` `table_name` - table name to be used for a model
 - `boolean` `uuid` - true if id type should be uuid instead of serial integer
 - `object` `api` API endpoints configutation
   - `object` (`admin` | `private` | `public`) - permission level
     - `object` (`get` | `post` | `put` | `delete`)
       - `boolean` `encrypt` - decrypt request parameters and encrypt responses
     - `boolean` (`get` | `post` | `put` | `delete`) - true to open endpoint with extra configuration
 - `string[]` `permit` - list of privilege groups to be permited access


### Types

 - `text`
   - `int` `min_length`
   - `int` `max_length`
   - `string` `regexp`
   - `boolean` `sanitize` - true to escape all potentially dangerous html (anti-XSS)
   - `object` `sanitize` - define all allowed tags and attributes instead
     - `string[]` `allow_tags`
     - `string[]` `allow_attrs`
 - `varchar(n)` where `n` is the length
   - `boolean` `hash`
   - `boolean` `password`
 - `int` or `bigint`
   - `int` `min`
   - `int` `max`
   - `string` `references` - name of the table for its items to be referenced (serial id).
   - `int` `default`
 - `uuid`
   - `string` `references` - name of the table for its items to be referenced (uuid).
 - `float`
   - `float` `min`
   - `float` `max`
   - `float` `default`
 - `double`
   - `double` `min`
   - `double` `max`
   - `double` `default`
 - `media`
   - `boolean` `multifile` - whether media column hold single or multiple files
   - `string[]` `mime_types` - allowed mime types. I.e. `video` or `video/mp4` if you want to only allow a specific format.


All types can have these properties:
 - `boolean` `unique`
 - `boolean` `required`

## API
By default all API endpoints are disabled and can be enabled within `api` proeperty of the model.
