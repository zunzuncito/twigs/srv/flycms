
export default class PrivateAPI {
  constructor(flycms) {
    const regexp = {
      get: new RegExp(`^\\/flycms-admin-api\\/([^?\\/]+)\\/?([0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}|\\d+)?(\\?.*)?$`),
      count: new RegExp(`^\\/flycms-admin-api\\/([^?\\/]+)\\/count(\\?.*)?$`),
      post: new RegExp(`^\\/flycms-admin-api\\/([^?\\/]+)(\\?.*)?$`),
      put: new RegExp(`^\\/flycms-admin-api\\/([^?\\/]+)\\/([0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}|\\d+)$`),
      delete: new RegExp(`^\\/flycms-admin-api\\/([^?\\/]+)\\/([0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}|\\d+)$`)
    }
    super(flycms, regexp);

    this.flycms = flycms;

    const flyauth = flycms.flyauth;

    const methods = ["get", "count", "post", "put", "delete"];
    for (let method of methods) {
      const http_method = method == "count" ? "get" : method;
      
      flycms.http[http_method](regexp[method], ...flyauth.default_middlewares, flyauth.authorize_mwfn, (req, res, next) => {
        const model_name = req.url.match(regexp[method])[1];
        const model = flycms.content_mg.models[model_name];

        if (model.allowed(req)) {
          next();
        } else {
          res.writeHead(401, "Unauthorized", {"Content-Type": "text/plain"});
          res.end("Unauthorized");
        }
      }, this[method]);

    }

  }

}
