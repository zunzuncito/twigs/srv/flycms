
import bcrypt from 'bcrypt'

import StringUtil from 'zunzun/flyutil/string.mjs'

import { JSDOM } from 'jsdom';
import DOMPurifier from 'dompurify';

import AES from 'zunzun/flypto/aes.mjs'

import QueryParser from 'zunzun/flycms/query/parser.mjs'

const window = new JSDOM('').window;
const DOMPurify = DOMPurifier(window);

const rand_charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

const valid = (value, colobj) => {
  if (colobj.required && (
    typeof value !=="number" && !value
  )) return false;
  if (colobj.type == "text" && colobj.regexp) {
    if (!value.match(new RegExp(colobj.regexp, "gm"))) return false;
  }
  return true;
}

const sanitize = (value, colobj) => {
  if (typeof colobj.sanitize == "object") {
    const pure_config = {
      ALLOWED_TAGS: colobj.sanitize.allow_tags 
    }
    if (colobj.sanitize.allow_attrs) {
      pure_config.ALLOWED_ATTR = colobj.sanitize.allow_attrs;
    }

    return DOMPurify.sanitize(value, pure_config);
  } else {
    return DOMPurify.sanitize(value);
  }
}


export default class API {
  constructor(flycms, regexp) {
    const _this = this;


    this.get = async (req, res, next) => {
      try {
        const url_match = req.url.match(regexp.get);

        const params = req.flyauth ? req.flyauth.params : req.params;
        const model_name = url_match[1];
        const item_id = url_match[2];
        const model = _this.flycms.content_mg.models[model_name];

        const encrypted = model.encrypted &&
          model.encrypted[req.flycms_context] &&
          model.encrypted[req.flycms_context].get;

        let columns = undefined;
        if (params.columns) {
          columns = params.columns.split(" ");
          for (let col of columns) {
            let valid = false;

            for (let ecol of model.table.columns) {
              if (ecol.name == col) {
                valid = true;
                break;
              }
            }

            if (!valid) {
              res.writeHead(422, {"Content-Type": "text/plain"});
              res.end("Unprocessable Entity");
              return;
            }
          }
        }

        if (!columns) columns = "*";

        if (!params.limit) params.limit = 50;

        let where_conditions = [];
        if (params.where) {
           where_conditions = QueryParser.conditions(decodeURIComponent(params.where));
        }

        if (typeof item_id == "number" || typeof item_id == "string") {
          where_conditions.push({
            column: "id",
            operator: "=",
            value: item_id,
            val_index: where_conditions.length+1
          });
        }

        let lang = model.lang;
/*        if (params.order) {
          if (params.order !== "id") {
            let col = model.get_column(params.order);
            if (col.lang) {
              lang = true;
            }
          }
        }*/

        if (params.search) {
          let keywords =
          where_conditions.push({
            column: "id",
            operator: "=",
            value: item_id,
            val_index: where_conditions.length+1
          });
        }

        console.log("params", params);


        let items = await model.select(columns, {
          offset: typeof params.offset !== "undefined" ? parseInt(params.offset) : undefined,
          limit: typeof params.limit !== "undefined" ? parseInt(params.limit) : undefined,
          order: typeof params.order !== "undefined" ? params.order : undefined,
          lang: lang ? req.cookies.language : undefined,
          desc: (params.desc == 'true' || params.desc == true),
          where: where_conditions
        });

        for (let i = where_conditions.length-1; i >= 0; i--) {
          const wcond = where_conditions[i];
          if (wcond.original_operator == "~") {
            items.sort((a, b) => {
              if (a.name[req.cookies.language].includes(wcond.value)) {
                if (b.name[req.cookies.language].includes(wcond.value)) {
                  return 0;
                } else {
                  return 1;
                }
              } else {
                if (b.name[req.cookies.language].includes(wcond.value)) {
                  return -1;
                } else {
                  return 0;
                }
              }
            });
            
            
          }
        }

        for (let c = 0; c < model.columns.length; c++) {
          if (typeof model.columns[c].references == "string") {
            const col_name = model.columns[c].name;
            const model_name = model.columns[c].references.slice(13);
            const model_inst = flycms.models[model_name];
            if (model_inst) {
              const option_text = model.columns[c].option_text;
              if (option_text) {
                for (let i = 0; i < items.length; i++) {
                  if (items[i][col_name]) {
                    const ref_id = items[i][col_name];
                    const ref_item = (await model_inst.select([option_text], {
                      where: `id = ${ref_id}`
                    }))[0];
                    items[i][col_name] = {
                      id: ref_id,
                      text: ref_item[option_text][req.cookies.language]
                    }
                  }
                }
              }
            }
          }
        }

/* TODO POPULATE
        const relations = {};
        const rel_insts = {};

        for (let c = 0; c < model.columns.length; c++) {
          if (typeof model.columns[c].references == "string") {
            const ref_name = model.columns[c].references.slice(13);
            const ref_inst = flycms.models[ref_name];
            if (ref_inst) {
              // TODO wanna optimize to select only needed ones
              relations[model.columns[c].name] = await ref_inst.select("*");
              rel_insts[model.columns[c].name] = ref_inst;
            }
          };
        }

        for (let i = 0; i < items.length; i++) {
          for (let rel in relations) { 
            if (typeof items[i][rel] != "undefined") {
               for (let rel_item of relations[rel]) {
                 if (rel_item.id+"" == items[i][rel]+"") {
                   items[i][rel] = rel_insts[rel].option_text ?
                     rel_item[rel_insts[rel].option_text] :
                     rel_item;
                 }
               }
            }
          }
        }*/

        res.writeHead(200, { 'Content-Type': 'application/json' });
        res.end(JSON.stringify(items));
      } catch (e) {
        console.error(e.stack);
      }
    }

    this.count = async (req, res, next) => {
      try {
        const model_name = req.url.match(regexp.count)[1];
        const model = _this.flycms.content_mg.models[model_name];
        const count = await model.table.aura.query(`SELECT COUNT(*) FROM ${model.table.name};`);
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.end(count.rows[0].count.toString());
      } catch (e) {
        console.error(e.stack);
      }
    }

    this.post = async (req, res, next) => {
      try {
        const model_name = req.url.match(regexp.post)[1];
        const model = _this.flycms.content_mg.models[model_name];

        const encrypted = model.api &&
          model.api[req.flycms_permissions] &&
          model.api[req.flycms_permissions].post &&
          model.api[req.flycms_permissions].post.encrypt;

        const hooks = model.api &&
          model.api[req.flycms_permissions] &&
          model.api[req.flycms_permissions].post &&
          model.api[req.flycms_permissions].post.hooks || {};



        const params = req.params = encrypted ? (
          JSON.parse(
            (req.flyauth ? req.flyauth.params : req.params).data
          )
        ) : (
            req.flyauth ? req.flyauth.params : req.params
        );

        let enc_key = undefined;
        if (encrypted) {
          req.aes_key = decodeURIComponent(params.enc_key);
          delete params.enc_key;
        }

        const columns = [];
        const expressions = [];
        const values = [];
        let cexpi = 1;


        for (let col in params) {
          const colobj = model.get_column(col);
          if (
            (colobj.type == "media" && colobj.multifile) ||
            colobj.pg_type == "text[]"
          ) {
            params[col] = params[col].split(", ");
          }

          if (colobj.type == "select" && colobj.pg_type == "bigint") {
            if (params[col] == "") {
              delete params[col];
              continue;
            }
          }

          if (colobj.type == "text" && colobj.random) {
            continue;
          }

          if (!valid(params[col], colobj)) {
            res.writeHead(422, "Unprocessable Content", {"Content-Type": "text/plain"});
            _this.respond(req, res, "Unprocessable Entity");
            return;
          }



          if (colobj.hash) {
            const salt = bcrypt.genSaltSync(10);
            params[col] = bcrypt.hashSync(params[col], salt);
          } else {
            if (colobj.type == "text" && colobj.pg_type !== "text[]") {
              if (colobj.lang) {
                for (let clang in params[col]) {
                  params[col][clang] = sanitize(params[col][clang], colobj);
                  params[col][clang] = params[col][clang].replace(/'/gm, "&#39;");
                }
              } else {
                if (colobj.sanitize) params[col] = sanitize(params[col], colobj);
                params[col] = params[col].replace(/'/gm, "&#39;");
              }
            }
          }

          if (
            (
              colobj.type == "int" ||
              colobj.type == "float" ||
              colobj.type == "double"
            ) &&
            typeof colobj.default == "number" &&
            (
              typeof params[col] == "undefined" || (
                typeof params[col] == "string" &&
                params[col].length == 0
              )
            )
          ) {
            params[col] = colobj.default;
          }

          if (
            !(
              (
                colobj.type == "int" ||
                colobj.type == "float" ||
                colobj.type == "double"
              ) &&
              typeof colobj.default == "number" &&
              (
                typeof params[col] == "undefined" || (
                  typeof params[col] == "string" &&
                  params[col].length == 0
                )
              )
            ) &&
            !(colobj.type == "text" && colobj.random) &&
            !(colobj.type == "date-time" && (
              colobj.auto == "created" ||
              colobj.auto == "edited"
            ))
          ) {
            columns.push(col);
            expressions.push(`$${cexpi}`);
            values.push(params[col]);
          }

          cexpi++;
        }

        const return_values = await model.insert(columns, expressions, values);


        if (typeof hooks["success"] == "function") {
          hooks["success"](req, res, () => {
            res.writeHead(200, "OK", { 'Content-Type': 'plain/text' });
            _this.respond(req, res, JSON.stringify(return_values));
          }, _this.flycms.sm)
        } else {
          res.writeHead(200, "OK", { 'Content-Type': 'plain/text' });
          _this.respond(req, res, JSON.stringify(return_values));
        }

      } catch (e) {
        console.error(e.stack);
      }
    }


    this.put = async (req, res) => {
      try {
        const url_match = req.url.match(regexp.put);
        const model_name = url_match[1];
        const model = _this.flycms.content_mg.models[model_name];

        const encrypted = model.encrypted &&
          model.encrypted[req.flycms_context] &&
          model.encrypted[req.flycms_context].put;

        const params = req.flyauth ? req.flyauth.params : req.params;

        const item_id = url_match[2];

        const expressions = [];
        const values = [];
        let cexpi = 1;

        for (let col in params) {
          const colobj = model.get_column(col);

          if (colobj.type == "select" && colobj.pg_type == "bigint") {
            if (params[col] == "") {
              delete params[col];
              continue;
            }
          }

          if (
            (colobj.type == "media" && colobj.multifile) ||
            colobj.pg_type == "text[]"
          ) {
            params[col] = params[col].split(", ");
          }

          if (!valid(params[col], colobj)) {
            res.writeHead(422, {"Content-Type": "text/plain"});
            res.end("Unprocessable Entity");
            return;
          }

          expressions.push(`${col} = $${cexpi}`);


          if (colobj.type == "text" && colobj.pg_type !== "text[]") {
            if (colobj.lang) {
              for (let clang in params[col]) {
                params[col][clang] = sanitize(params[col][clang], colobj);
                params[col][clang] = params[col][clang].replace(/'/gm, "&#39;");
              }
            } else {
              if (colobj.sanitize) params[col] = sanitize(params[col], colobj);
              params[col] = params[col].replace(/'/gm, "&#39;");
            }
          }

          
          if (
            (
              colobj.type == "int" ||
              colobj.type == "float" ||
              colobj.type == "double"
            ) && 
            typeof colobj.default == "number" &&
            (
              typeof params[col] == "undefined" || (
                typeof params[col] == "string" &&
                params[col].length == 0
              )
            )
          ) {
            params[col] = colobj.default;
          }

          values.push(params[col]);
          cexpi++;
        }

        const return_values = {};

        for (let colobj of model.columns) {
          if (colobj.type == "date-time" && colobj.auto == "edited") {
            const date_time = new Date().toISOString().slice(0, 19).replace('T', ' ');
            expressions.push(`${colobj.name} = $${cexpi}`);
            values.push(date_time);
            cexpi++;
            return_values[colobj.name] = date_time;
          }
        }

        return_values["id"] = item_id;
        values.push(item_id);

        await model.table.update(expressions, `id = $${cexpi}`, values);

        res.writeHead(200, "OK", { 'Content-Type': 'plain/text' });
        res.end(req.oaes.encrypt(JSON.stringify(return_values)));
      } catch (e) {
        console.error(e.stack);
      }
    }

    this.delete = async (req, res) => {
      try {
        const url_match = req.url.match(regexp.delete);
        const model_name = url_match[1];
        const model = _this.flycms.content_mg.models[model_name];

        const encrypted = model.encrypted &&
          model.encrypted[req.flycms_context] &&
          model.encrypted[req.flycms_context].put;

        let ids = [];
        let ins = [];
        let cin = 1;
        if (url_match[2] == "ids") {
          for (let id of req.params.data.ids) {
            ids.push(id);
            ins.push(`$${cin}`);
            cin++;
          }

        } else {
          ids.push(url_match[2]);
          ins.push(`$${cin}`);
          cin++;
        }
        await model.table.delete(`id IN (${ins.join(', ')})`, ids);

        res.writeHead(200, "OK");
        res.end("OK");
      } catch (e) {
        console.error(e.stack);
      }

    }

  }

  async respond(req, res, response) {
    try {
      if (req.oaes) {
        res.end(req.oaes.encrypt(response));
      } else {
        res.end(response);
      }
    } catch (e) {
      console.error(e.stack);
    }
  }
}
