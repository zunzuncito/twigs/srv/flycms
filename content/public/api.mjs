
import API from '../api.mjs'

export default class PublicAPI extends API {
  constructor(flycms, cfg) {
    const regexp = {
      get: new RegExp(`^\\/flycms-api\\/([^?\\/]+)\\/?([0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}|\\d+)?(\\?.*)?$`),
      count: new RegExp(`^\\/flycms-api\\/([^?\\/]+)\\/count(\\?.*)?$`),
      post: new RegExp(`^\\/flycms-api\\/([^?\\/]+)(\\?.*)?$`),
      put: new RegExp(`^\\/flycms-api\\/([^?\\/]+)\\/([0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}|\\d+)$`),
      delete: new RegExp(`^\\/flycms-api\\/([^?\\/]+)\\/([0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}|\\d+)$`)
    }

    super(flycms, regexp);

    this.flycms = flycms;

    const flyadmin = flycms.flyadmin;
    const flyauth = flycms.flyauth;

    const methods = ["get", "count", "post", "put", "delete"];

    for (let method of methods) {
      const http_method = method == "count" ? "get" : method;
      flycms.http[http_method](regexp[method], ...flyauth.default_middlewares, flyauth.forward_credentials_mwfn, (req, res, next) => {
        const model_name = req.url.match(regexp[method])[1];
        const model = flycms.content_mg.models[model_name];
        if (model.api && model.api.public && model.api.public[method]) {
          req.flycms_permissions = "public";
          next()
        } else {
          res.writeHead(401, "Unauthorized", {"Content-Type": "text/plain"});
          res.end("Unauthorized");
        }
      }, this[method]);
    }
  }

}
