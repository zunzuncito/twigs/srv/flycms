const conditional_characters = [
  "=", "!", "<", ">"
];

const conditional_operators = [
  "=", "!=", "<", "<=", ">", ">="
];

export default class QueryParser {
  static condition_stack(str, val_index) {
    const stack = [];
    let cexpr = '';
    let nexpr = 0;
    let qopen = false;
    if (typeof val_index !== "object") val_index = { num: 0 };
    for (let c = 0; c < str.length; c++) {
      const char = str[c];
      if (char === '(' && !qopen) {
        if (nexpr == 0) {
          if (cexpr.length > 0) stack.push(QueryParser.single_condition(cexpr, val_index));
          cexpr = '';
        } else {
          cexpr += char;
        }
        nexpr++;
      } else if (char === ')' && !qopen) {
        if (nexpr == 1) {
          stack.push(QueryParser.condition_stack(cexpr, val_index));
          cexpr = '';
        } else {
          cexpr += char;
        }
        nexpr--;
      } else if (char === "'") {
        qopen = !qopen;
        cexpr += char;
      } else if ((char === '|' || char === '&') && nexpr == 0 && !qopen) {
        if (cexpr.length > 0) stack.push(QueryParser.single_condition(cexpr, val_index));
        stack.push(char);
        cexpr = '';
      } else {
        cexpr += char;
      }
    }
    if (cexpr.length > 0) stack.push(QueryParser.single_condition(cexpr, val_index));

    return stack;
  }

  static single_condition(str, val_index) {
    val_index.num++;
    const match_results = str.match(/([\w]+)(?:[\s]+)?(=|!=|<=|>=|<|>)(?:[\s]+)?('.*'|[^ ]+)(?:[\s]+)?/);
    const column = match_results[1];
    const operator = match_results[2];
    const value = match_results[3];
    return {
      column: column,
      operator: operator,
      val_index: val_index.num,
      value: typeof value === "string" &&
        value.startsWith(`'`) &&
        value.endsWith(`'`) ?
        value.slice(1, -1) : value
    };
    
  }

  static stack_to_psql(exprs, layer) {
    if (!layer) layer = 0;

    let psql_condition = '';

    if (layer > 0) {
      psql_condition += "("
    }

    for (let expr of exprs) {
      if (expr === "|") {
        psql_condition += " OR ";
      } else if (expr === "&") {
        psql_condition += " AND ";
      } else if (Array.isArray(expr)) {
        psql_condition += QueryParser.stack_to_psql(expr, layer+1);
      } else {
        psql_condition += QueryParser.single_condition(expr);
      }
    }

    if (layer > 0) {
      psql_condition += ")"
    }
    
    return psql_condition;
  }

  static conditions(str) {
    const stack = QueryParser.condition_stack(str);

    return stack;

  }
}
