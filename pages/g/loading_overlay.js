
let loading_screen_block = document.getElementById('loading_overlay');

let limgr = loading_screen_block.querySelector("img.loading");
limgr.onload = () => {
  limgr.classList.remove("hidden");
}


function display_loading_overlay() {
  loading_screen_block.style.display = "";
  loading_screen_block.style.opacity = 1;
  disableScroll();
}

function hide_loading_overlay() {
  console.log('remove_overlay');
  setTimeout(function() {
    let last_time = undefined;
    let delta = 1;

    loading_screen_block.style.opacity = 1;
    let interval = setInterval(function() {
      var new_time = Date.now();
      if (last_time) {
        delta = new_time-last_time;
      }

      loading_screen_block.style.opacity -= 0.005*delta;

      if (loading_screen_block.style.opacity < 0) {
        clearInterval(interval);
        loading_screen_block.style.display = "none";
        enableScroll();
      }

      last_time = new_time;
    }, 1);
  }, 333);
}
display_loading_overlay();

// left: 37, up: 38, right: 39, down: 40,
// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
var keys = {37: 1, 38: 1, 39: 1, 40: 1};

function preventDefault(e) {
  e = e || window.event;
  if (e.preventDefault)
      e.preventDefault();
  e.returnValue = false;
}

function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
        preventDefault(e);
        return false;
    }
}

function disableScroll() { 
  document.body.style.overflow = 'hidden';
  if (window.addEventListener) // older FF
      window.addEventListener('DOMMouseScroll', preventDefault, false);
  window.onwheel = preventDefault; // modern standard
  window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
  window.ontouchmove  = preventDefault; // mobile
  document.onkeydown  = preventDefaultForScrollKeys;
}

function enableScroll() {
  if (window.removeEventListener)
    window.removeEventListener('DOMMouseScroll', preventDefault, false);
  window.onmousewheel = document.onmousewheel = null;
  window.onwheel = null;
  window.ontouchmove = null;
  document.onkeydown = null;
  document.body.style.overflow = 'visible';
}

