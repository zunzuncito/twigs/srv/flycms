
export default class Middleware {

  static async render(req, res, njk, ctx, sm) {
    try {
      const flycms = sm.get_service_instance("flycms");

      console.log(req.url);

      ctx.header_menu = [];

      for (let model in flycms.models) {
        if (req.flyauth && flycms.models[model].allowed(req)) {
          console.log("model", model);
          console.log("model", flycms.models[model].name);
          const menu_item = {
            text: flycms.models[model].name,
            href: `/flycms/model/${model}/index.html`,
            name: model,
            table_name: flycms.models[model].table_name

          };
          if (typeof menu_item.text == "object") {
            menu_item.lang = menu_item.text
          }
          ctx.header_menu.push(menu_item);
        }
      }


      ctx.header_menu.sort((a, b) => { return a.text.localeCompare(b.text) });

      return njk;
    } catch (e) {
      console.error(e.stack);
      return njk;
    }
  }

}
