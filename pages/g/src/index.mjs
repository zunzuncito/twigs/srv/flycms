

import SelectInput from "zunzun/flyhtml/input/select.mjs"
import CredParser from 'zunzun/authing.ui/cred/parser.mjs'
import Fingerprint from 'zunzun/flyfp.web/index.mjs'

const fp_base = 1.5;

window.addEventListener("flyload", async function(event) {
  try {
    const models_select = document.getElementById("models-select");
    const model_input = (new SelectInput(models_select, {
      dont_hide: true
    })).input;

    const dev_fp = Fingerprint.get(fp_base);
    const fp_salt = Fingerprint.get_salt();
    const cred_parser = new CredParser(dev_fp, fp_salt);
    const creds = await cred_parser.next();
    let model_selected = false;
    model_input.addEventListener('change', (e) => {
      model_selected = true;
      window.location.href = `${model_input.real_value}?cks=${creds.cks}&authorized=${creds.authorized}`
    });

    model_input.addEventListener("mouseover", (e) => {
      model_input.focus();
    });

    model_input.addEventListener("focus", (evt) => {
      model_input.parentNode.parentNode.classList.add("focused");
    });
    model_input.addEventListener("blur", (evt) => {
      if (!model_selected) model_input.parentNode.parentNode.classList.remove("focused");
    });
  } catch (e) {
    console.error(e);
  }
});
