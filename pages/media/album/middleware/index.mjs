
import fs from 'fs'
import path from 'path'

import FSUtil from 'zunzun/flyutil/fs.mjs'


export default class Middleware {

  static async render(req, res, njk, ctx, sm) {
    try {
      const flycms = sm.get_service_instance("flycms");

      const name = req.url.match(/^\/flycms\/media\/album\/([^?\/]+)(\/|\/index\.html)?/)[1];
      const title = flycms.models[name].name;
      ctx.title = `Album: ${title}`;

      const album_path = path.resolve(flycms.albums_path, name)
      if (!fs.existsSync(album_path)) FSUtil.force_mkdir(album_path);

      const total_items = flycms.albums[name].count();
      ctx.page_size = 12;
      ctx.total_item_count = total_items;
      ctx.page_count = Math.ceil(total_items/ctx.page_size);

      ctx.album_title = title;
      ctx.album_name = name;

      ctx.media = await flycms.albums[name].get_page(0, ctx.media_page_size);

//      ctx.media_items = await flycms.albums[name].get_page(0, ctx.page_size);

     /* 

      const media_files = fs.readdirSync(album_path);

      ctx.album_title = title;
      ctx.album_name = name;
      ctx.media_items = [];
      for (let mfile of media_files) {
        const mfile_path = path.resolve(album_path, mfile);
        const stream = fs.createReadStream(mfile_path);
        const file_type = await fileTypeFromStream(stream);

        ctx.media_items.push({
          file: mfile,
          mime: file_type.mime.split("/")[0]
        });
      }*/


      return njk;
    } catch (e) {
      console.error(e.stack);
      return njk;
    }
  }

}
