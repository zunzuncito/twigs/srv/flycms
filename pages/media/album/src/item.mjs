
import CredParser from 'zunzun/authing.ui/cred/parser.mjs'
import Fingerprint from 'zunzun/flyfp.web/index.mjs'

const fp_base = 1.5;

const dev_fp = Fingerprint.get(fp_base);
const fp_salt = Fingerprint.get_salt();
const cred_parser = new CredParser(dev_fp, fp_salt);

const video_formats = ["mp4", "ogv", "webm"];

export default class MediaItem {
  constructor(mi, paginator) {


    const mi_name = mi.name || mi.getAttribute("name");
    const remove_btn = mi.querySelector("div.remove");
    remove_btn.addEventListener("click", async (e) => {
      if (confirm(`Are you sure you want to remove "${album_name}/${mi_name}"?`)) {
        display_loading_overlay();
        const creds = await cred_parser.next();
        await fetch(`/flycms-media-remove/${album_name}/${mi_name}`, {
          method: "DELETE",
          headers: {
            "Authorization": `cks=${creds.cks}&authorized=${creds.authorized}`
          }
        })

        const next_creds = await cred_parser.next({
          offset: current_page_index*page_size + page_size - 1,
          limit: 1
        });
        const page_media = await (await fetch(`/flycms-media-get/${album_name}?cks=${next_creds.cks}&authorized=${next_creds.authorized}`)).json();
        for (let media_item of page_media) {
          await MediaItem.construct(media_item.file, media_item.mime, mi.parentNode, paginator);
        }


        total_item_count--;
        await paginator.resize(Math.ceil(total_item_count/page_size));

        if (mi.parentNode) mi.parentNode.removeChild(mi);

        hide_loading_overlay();
      }
    });
  }

  static async construct(file_name, file_type, media_items_div, paginator) {
    try {

      const new_media_div = document.createElement("div");
      new_media_div.classList.add("media-item");
      new_media_div.setAttribute("name", file_name);

      const top_bar_div = document.createElement("div");
      top_bar_div.classList.add("top-bar");

      const top_name = document.createElement("p");
      top_name.classList.add("name");
      top_name.innerHTML = file_name;
      top_bar_div.appendChild(top_name);

      const top_remove = document.createElement("div");
      top_remove.classList.add("remove");
      top_remove.innerHTML = "&#xF10C;";
      top_bar_div.appendChild(top_remove);

      new_media_div.appendChild(top_bar_div);


      if (file_type.startsWith("video")) {
        const video_block = document.createElement("video");
        video_block.setAttribute("controls", "controls");
        for (const vformat of video_formats) {
          const mcreds = await cred_parser.next();
          const src_name = vformat == "mp4" ? file_name : file_name.replace(/mp4$/, vformat);
          const video_source = document.createElement("source");
          video_source.src = `/flycms/media/items/${album_name}/${src_name}?cks=${mcreds.cks}&authorized=${mcreds.authorized}`;
          const vid_type = vformat == "ogv" ? file_type+"/ogg" : file_type+"/"+vformat;
          video_source.setAttribute("type", vid_type);
          video_block.appendChild(video_source);
        }
        new_media_div.appendChild(video_block);
      } else if (file_type.startsWith("audio")) {
        const mcreds = await cred_parser.next();
        const video_block = document.createElement("audio");
        video_block.setAttribute("controls", "controls");
        const video_source = document.createElement("source");
        video_source.src = `/flycms/media/items/${album_name}/${file_name}?cks=${mcreds.cks}&authorized=${mcreds.authorized}`;
        video_source.setAttribute("type", file_type);
        video_block.appendChild(video_source);
        new_media_div.appendChild(video_block);
      } else if (file_type.startsWith("image")) {
        const mcreds = await cred_parser.next();
        const image_block = document.createElement("img");
        image_block.src = `/flycms/media/items/${album_name}/${file_name}?cks=${mcreds.cks}&authorized=${mcreds.authorized}`;
        new_media_div.appendChild(image_block);
      }

      media_items_div.appendChild(new_media_div);

      return new MediaItem(new_media_div, paginator);
    } catch (e) {
      console.error(e);
    }
  }
}
