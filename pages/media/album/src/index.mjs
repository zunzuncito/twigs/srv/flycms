
import MediaItem from './item.mjs'

import CredParser from 'zunzun/authing.ui/cred/parser.mjs'
import Fingerprint from 'zunzun/flyfp.web/index.mjs'

import PaginatorUI from 'zunzun/paginator.ui/index.mjs'

import MediaUpload from 'zunzun/flycms.ui/media/upload.mjs'

import MediaSelect from 'zunzun/flycms.ui/media/select.mjs'


const fp_base = 1.5;

window.addEventListener("flyload", async (evt) => {
  try {
    const cred_parser = AUTHING_CREDENTIALS.parser;

    const select_media_items = document.getElementById("select-media-items");

    const media_select = await MediaSelect.construct(
      null,
      select_media_items,
      cred_parser,
      {
        sort: "date",
        desc: true,
        single_page: true,
        total_items: total_item_count
      }
    );


    const delete_btn = document.getElementById("delete-btn");
    delete_btn.addEventListener("click", async (e) => {
      try {
        const delete_items = media_select.get_value().split(", ");

        let confirm_msg = `${LANG.rm_items}\n`;
        for (let item of delete_items) {
          confirm_msg += item+"\n";
        }

        if (confirm(confirm_msg)) {
          display_loading_overlay();

          for (let item of delete_items) {
            const creds = await cred_parser.next();
            await fetch(`/flycms-media-remove/${album_name}/${item}`, {
              method: "DELETE",
              headers: {
                "Authorization": `cks=${creds.cks}&authorized=${creds.authorized}`
              }
            })

            media_select.total_items--;

          }

          media_select.set_value("");


          await media_select.paginator.resize(Math.ceil(media_select.total_items / page_size));
          await media_select.paginator.select_page(); 

          hide_loading_overlay();
        }
      } catch (e) {
        console.error(e);
      }
    });


    const select_all_btn = document.getElementById("select-all-btn");
    select_all_btn.addEventListener("click", (e) => {
      media_select.sgi.select_all();
    });
    const deselect_all_btn = document.getElementById("deselect-all-btn");
    deselect_all_btn.addEventListener("click", (e) => {
      media_select.sgi.deselect_all();
    });

    const upload_block = document.body.querySelector("div.media-upload");
    const upload_input = upload_block.querySelector("input[type=file]");
    new MediaUpload(upload_input, cred_parser, async (files) => {
      //media_select.new_files = files;
      media_select.total_items += files.length;
      await media_select.paginator.resize(Math.ceil(media_select.total_items / page_size));
      await media_select.paginator.select_page(0);
    });

    const e_sort_value = document.getElementById("sort-value");
    e_sort_value.addEventListener('change', async (e) => {
      media_select.sort = e_sort_value.value;
      await media_select.reload();
    });

    const e_sort_desc = document.getElementById("sort-desc");
    e_sort_desc.addEventListener('click', async (e) => {
      if (media_select.desc) {
        media_select.desc = false;
        e_sort_desc.innerHTML = "&#xF103;";
      } else {
        media_select.desc = true;
        e_sort_desc.innerHTML = "&#xF104;";
      }
      await media_select.reload();
    });


  //  const media_items = media_items_div.querySelectorAll("div.media-item");
/*
    const upload_input = document.getElementById("mediaUpload");
    new MediaUpload(upload_input, cred_parser, async () => {
      const media_item_divs = media_items_div.querySelectorAll("div.media-item");

      let new_items = [];
      for (let mi_div of media_item_divs) {
        new_items.push({
          name: mi_div.getAttribute("name"),
          existing: mi_div
        });
        media_items_div.removeChild(mi_div);
      }

      for (let i = 0; i < upload_input.files.length; i++) {
        const file_name = upload_input.files[i].name;
        const file_type = upload_input.files[i].type;
        new_items.push({
          name: file_name,
          type: file_type
        });
      }

      new_items.sort((a, b) => a.name.localeCompare(b.name));
      new_items = new_items.slice(0, page_size);

      for (let nitem of new_items) {
        if (nitem.existing) {
          media_items_div.appendChild(nitem.existing);
        } else {
          await MediaItem.construct(nitem.name, nitem.type.split("/")[0], media_items_div, paginator)
        }
      }

      total_item_count += upload_input.files.length;
      paginator.resize(Math.ceil(total_item_count/page_size));
    });

    const media_pages = document.getElementById("media-pages");

    const paginator = new PaginatorUI({
      parent_element: media_pages,
      page_size: page_size,
      page_count: page_count
    }, async function(page_index, paginator_instance) {
      try {
        current_page_index = page_index - 1;
        media_items_div.innerHTML = "";
        const creds = await cred_parser.next({
          offset: (page_index-1)*page_size,
          limit: page_size
        });
        const page_media = await (await fetch(`/flycms-media-get/${album_name}?cks=${creds.cks}&authorized=${creds.authorized}`)).json();
        for (let media_item of page_media) {
          await MediaItem.construct(media_item.file, media_item.mime, media_items_div, paginator)
        }

        const media_items_e = media_items_div.getElementsByClassName("media-item");

        hide_loading_overlay();
      } catch (e) {
        console.error(e);
      }
    });
    await paginator.resize(page_count);
  
    for (let mi of media_items) {
      new MediaItem(mi, paginator);
    }
    media_items_div.parentNode.removeChild(media_items_div);

    paginator.display.appendChild(media_items_div);
    await paginator.select_page(0);
*/
  //  hide_loading_overlay();
  } catch (e) {
    console.error(e);
  }
});
