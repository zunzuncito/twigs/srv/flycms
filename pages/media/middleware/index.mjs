
export default class Middleware {

  static async render(req, res, njk, ctx, sm) {
    try {
      const flycms = sm.get_service_instance("flycms");

      ctx.albums = [];

      for (let album in flycms.albums) {
        ctx.albums.push({
          name: flycms.albums[album].name,
          api_name: album
        });
      }
      console.log(ctx.albums);

      return njk;
    } catch (e) {
      console.error(e.stack);
      return njk;
    }
  }

}
