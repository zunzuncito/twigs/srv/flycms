
import FlyAuth from 'zunzun/authing.web/index.mjs'

const flyauth_div = document.querySelector("div.flyauth");

(async () => {
  try {
    const flyauth = new FlyAuth.SignIn({
      success_path: "/flycms",
      path_prefix: "/flycms",
      instructions: false,
      lang: LANG,
      current_language: CURRENT_LANGUAGE
    })
    flyauth_div.appendChild(flyauth.element);
  } catch (e) {
    console.error(e);
  }
})();
