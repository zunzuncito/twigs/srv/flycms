
import ContentEditor from "./content/editor.mjs"
import ContentItem from './content/item.mjs'
import ContentTable from "zunzun/flyhtml/content/table.mjs"

import CredParser from 'zunzun/authing.ui/cred/parser.mjs'
import Fingerprint from 'zunzun/flyfp.web/index.mjs'

import RichTextEditor from 'zunzun/flyhtml/rich-text/edit.mjs'
import SelectGridInput from 'zunzun/flyhtml/input/select-grid.mjs'

import PaginatorUI from 'zunzun/paginator.ui/index.mjs'

import MediaSelect from 'zunzun/flycms.web/media/select.mjs'


import MediaUpload from 'zunzun/flycms.web/media/upload.mjs'


const fp_base = 1.5;

const video_formats = ["mp4", "ogv", "webm"];

window.addEventListener("flyload", async (evt) => {
  try {
    const dev_fp = Fingerprint.get(fp_base);
    const fp_salt = Fingerprint.get_salt();
    const cred_parser = new CredParser(dev_fp, fp_salt);

    const media_pages = document.getElementById("media-pages");

    const select_media_items = document.getElementById("select-media-items");

    const rich_editors = document.getElementsByClassName("rich-text-input");
    for (let rich_editor of rich_editors) {
      const rte = new RichTextEditor(rich_editor);
      let rich_sgi = new SelectGridInput(rte.media_select, select_media_items, true);
      rte.media_cb = async () => {
        try {
          await new Promise((fulfil) => {
            const wait_media_int = setInterval(() => {
              if (!rich_sgi.options_div.parentNode) {
                clearInterval(wait_media_int);
                fulfil();
              }
            }, 100);
          });
          for (let v = 0; v < rich_sgi.values.length; v++) {
            rich_sgi.values[v] = `/g/albums/${album_name}/`+rich_sgi.values[v];
          }

          console.log("RTRN", rich_sgi.values);
          return rich_sgi.values;
        } catch (e) {
          console.error(e.stack);
        }
      }
    }


    const select_inputs = document.getElementsByClassName("select-media");

//    new ContentTable(content_model.columns);


    let options_div = undefined;
    const sgis = [];
//    for (let si of select_inputs) {
    for (let select_input of select_inputs) {

      const media_select = await MediaSelect.construct(
        select_input,
        select_media_items,
        cred_parser,
        {
          sort: "date",
          desc: true
        }
      );

      const close_btn = document.createElement("button");
      close_btn.classList.add("close");
      close_btn.innerHTML = "X";
      close_btn.addEventListener("click", (e) => {
        media_select.hide();
      });
      media_select.sgi.element.insertBefore(close_btn, media_select.paginator.element);

      const upload_block = document.createElement("div");
      upload_block.classList.add("select-grid-upload");
      const multifile = media_select.sgi.multifile ? `multiple="multiple" ` : '';
      upload_block.innerHTML = `
      <label for="media-upload">${LANG.upload.instruct}</label>
      <input type="file" id="media-upload" class="media-upload" name="file" accept="image/*, audio/*, video/*" ${multifile}autocomplete=off>
      <div class="selected-files"></div>
      <input type="submit" value="${LANG.upload.submit}" disabled=disabled>
      `;
      const upload_input = upload_block.querySelector("input[type=file]");
      new MediaUpload(upload_input, cred_parser, async (files) => {
        media_select.new_files = files;
        await media_select.paginator.select_page(0);
      });
      media_select.sgi.element.insertBefore(upload_block, media_select.paginator.element);

      const submit_btn = document.createElement("button");
      submit_btn.classList.add("submit");
      submit_btn.innerHTML = "Submit";
      submit_btn.addEventListener("click", (e) => {
        select_input.value = media_select.get_value();
        media_select.hide();
      });
      media_select.sgi.element.appendChild(submit_btn);
    }
/*

      const sgi = new SelectGridInput(
        si,
        select_media_items,
        si.classList.contains("multifile") ? true : false
      );
      if (!options_div) options_div = sgi.options_div;
      sgis.push(sgi);

      const grid_close = sgi.options_div.querySelector("div.select-grid-close");
      grid_close.innerHTML = "&#xF10A;";
      const grid_submit = sgi.options_div.querySelector("div.select-grid-submit");
      grid_submit.innerHTML = "&#xF10D;";*/
//    }

    if (select_inputs.length > 0) {/*
      const paged_media = new PaginatorUI({
        parent_element: media_pages,
        page_size: media_page_size,
        page_count: media_page_count
      }, async function(page_index, paginator_instance) {
        try {
          select_media_items.innerHTML = "";
          const creds = await cred_parser.next({
            offset: (page_index-1)*media_page_size,
            limit: media_page_size
          });
          const page_media = await (await fetch(`/flycms-media-get/${album_name}?cks=${creds.cks}&authorized=${creds.authorized}`)).json();
          for (let media_item of page_media) {
            const media_item_div = document.createElement("div");
            media_item_div.classList.add("select-media-item");
            media_item_div.setAttribute("value", media_item.file); // .mime

            const media_header_div = document.createElement("div");
            media_header_div.classList.add("media-item-header");
            media_header_div.innerHTML = media_item.file;
            media_item_div.appendChild(media_header_div);

            if (media_item.mime.startsWith("image")) {
              const media_img = document.createElement("img");
              const item_creds = await cred_parser.next();
              media_img.src = `/flycms/media/items/${album_name}/${media_item.file}?cks=${item_creds.cks}&authorized=${item_creds.authorized}`;
              media_item_div.appendChild(media_img);
            } else if (media_item.mime.startsWith("video")) {
              const media_video = document.createElement("video");
              media_video.setAttribute("controls", "controls");
              for (const vformat of video_formats) {
                const mcreds = await cred_parser.next();
                const src_name = vformat == "mp4" ? media_item.file : media_item.file.replace(/mp4$/, vformat);
                const video_source = document.createElement("source");
                video_source.src = `/flycms/media/items/${album_name}/${src_name}?cks=${mcreds.cks}&authorized=${mcreds.authorized}`;
                const vid_type = vformat == "ogv" ? media_item.mime+"/ogg" : media_item.mime+"/"+vformat;
                video_source.setAttribute("type", vid_type);
                media_video.appendChild(video_source);
              }
              media_item_div.appendChild(media_video);
            } else if (media_item.mime.startsWith("audio")) {
              const media_video = document.createElement("audio");
              media_video.setAttribute("controls", "controls");
              const source = document.createElement("source");
              source.type = media_item.mime;
              const item_creds = await cred_parser.next();
              source.src = `/flycms/media/items/${album_name}/${media_item.file}?cks=${item_creds.cks}&authorized=${item_creds.authorized}`;
              media_video.appnedChild(source);
              media_item_div.appendChild(media_video);
            }

            let sgi = SelectGridInput.get_current_instance();
            sgi.last_page = page_index-1;
            sgi.add_item(media_item_div);
            select_media_items.appendChild(media_item_div);
          }


          hide_loading_overlay();
        } catch (e) {
          console.log(e);
        }
      });
      await paged_media.resize(media_page_count);

      select_media_items.parentNode.removeChild(select_media_items);
      paged_media.display.appendChild(select_media_items);
      paged_media.element.parentNode.removeChild(paged_media.element);;
      options_div.appendChild(paged_media.element);

      for (let sgi of sgis) {
        sgi.on_focus = async () => {
          try {
            if (typeof sgi.last_page !== "number") sgi.last_page = 0;
            await paged_media.select_page(sgi.last_page);
          } catch (e) {
            console.error(e);
          }
        }
      }


    */}



    let selected_rows = [];


    const table_columns = [ ...content_model.columns ];

    for (let tc of table_columns) {
      tc.resize = true;
      tc.wrap = true;
      if (typeof tc.title == "object") {
        tc.title = tc.title[current_language]
      }
    }

    table_columns.unshift({
      name: "select",
      title: "&#xF102;",
      type: "option",
      value_cb: (row) => {
        const checkbox = document.createElement("input");
        checkbox.addEventListener("change", (e) => {
          row.selected = checkbox.value;
          for (let row of content_table.rows) {
            if (row.checkbox == checkbox) {
              if (checkbox.checked) {
                row.selected = true;
                row.checkbox.checked = true;
                selected_rows.push(row);
              } else {
                row.selected = false;
                row.checkbox.checked = false;
                selected_rows.splice(
                  selected_rows.indexOf(row),
                  1
                );
              }
            }
          }
          console.log("selected_rows", selected_rows)
        });
        checkbox.type = "checkbox";
        row.checkbox = checkbox;
        return checkbox;
      }
    }, {
      name: "id",
      title: "Id",
      type: content_model.uuid ? "text" : "int",
      resize: true,
      wrap: true
    });

    table_columns.push({
      name: "edit",
      title: "&#xF109;",
      type: "option",
      value_cb: (row) => {
        const new_item_edit = document.createElement("button");
        new_item_edit.classList.add("edit");
        new_item_edit.innerHTML = "&#xF109;";
        new_item_edit.addEventListener("click", (e) => {
          content_editor.edit(row);
        });
        return new_item_edit;
      }
    }, {
      name: "delete",
      title: "&#xF108;",
      type: "option",
      value_cb: (row) => {
        const new_item_edit = document.createElement("button");
        new_item_edit.classList.add("delete");
        new_item_edit.innerHTML = "&#xF108;";
        new_item_edit.addEventListener("click", async (e) => {
          const ids = [row.values.id];


          let confirm_msg = "Are you sure you want to remove the following items?\n";
          confirm_msg += ids.join(", ");

          if (confirm(confirm_msg)) {
            display_loading_overlay();

            const creds = await cred_parser.next({ ids: ids });
            await fetch(`/flycms-admin-api/${content_table.name}/ids`, {
              method: "DELETE",
              headers: {
                "Authorization": `cks=${creds.cks}&authorized=${creds.authorized}`
              }
            });

            total_item_count--;

            await content_editor.paginator.resize(Math.ceil(total_item_count/page_size));
            await content_editor.paginator.select_page(content_editor.paginator.cur_page.index);

            hide_loading_overlay();
          }

        });
        return new_item_edit;
      }
    });

    const content_table = new ContentTable(content_model.api_name, table_columns, {
      "language": current_language
    });

    content_table.select_all = () => {
      for (let row of content_table.rows) {
        row.selected = true;
        row.checkbox.checked = true;
        selected_rows.push(row);
      }
    }

    content_table.deselect_all = () => {
      for (let row of content_table.rows) {
        row.selected = false;
        row.checkbox.checked = false;
        selected_rows = [];
      }
    };

    /*content_table.get_selected = () => {
      const list = [];

      for (let row of content_table.rows) {
        if (row.selected) {
          list.push(row);
        } 
      }

      return list;
    };*/

    const content_table_div = document.body.querySelector("div.content-table");
    content_table_div.appendChild(content_table.element);

    const item_editor = document.getElementById("item-editor");
    const content_editor = new ContentEditor(item_editor, content_table, cred_parser);
    content_table.editor= content_editor;

    const new_item_opt = document.getElementById("new-item-opt");
    new_item_opt.addEventListener("click", async (e) => {
      content_editor.edit();
    });

    const model_pages = document.getElementById("model-pages");

    console.log("PAGINATE", content_model);
    const paginator = content_editor.paginator = content_table.paginator = new PaginatorUI({
      parent_element: model_pages,
      page_size: page_size,
      page_count: page_count
    }, async function(page_index, paginator_instance) {
      try {
        paginator.display.appendChild(content_table_div);
        current_page_index = page_index-1;
        if (current_page_index < 0) current_page_index = 0;
        console.log("current_page_index", current_page_index);

        content_table.clear();

        const creds = await cred_parser.next({
          offset: current_page_index*page_size,
          limit: page_size,
          order: order.column,
          desc: order.desc
        });
        const model_page = await (await fetch(`/flycms-admin-api/${content_model.api_name}?cks=${creds.cks}&authorized=${creds.authorized}`)).json();
        for (const pmodel of model_page) {
          content_table.add(pmodel);
        }
        console.log(model_page);

        content_table.prepare();
        content_table.fit();

        hide_loading_overlay();
      } catch (e) {
        console.log(e);
      }
    });
    await paginator.resize(page_count);



    const delete_btn = document.getElementById("delete-btn");
    delete_btn.addEventListener("click", async (e) => {
      try {

        const delete_items = [];
        
          console.log("selected_rows", selected_rows);
        for (let row of selected_rows) {
          
          delete_items.push(row.values.id);

        }
        

        let confirm_msg = "Are you sure you want to remove the following items?\n";
        confirm_msg += delete_items.join(", ");

        if (confirm(confirm_msg)) {
          display_loading_overlay();

          console.log("delete_items", delete_items);
          const creds = await cred_parser.next({ ids: delete_items });
          await fetch(`/flycms-admin-api/${content_table.name}/ids`, {
            method: "DELETE",
            headers: {
              "Authorization": `cks=${creds.cks}&authorized=${creds.authorized}`
            }
          });

          total_item_count -= delete_items.length;

          await content_editor.paginator.resize(Math.ceil(total_item_count/page_size));
          await content_editor.paginator.select_page(content_editor.paginator.cur_page.index);

          await content_editor.paginator.select_page(0);

          selected_rows = [];
          hide_loading_overlay();
        }
      } catch (e) {
        console.error(e);
      }
    });


    const select_all_btn = document.getElementById("select-all-btn");
    select_all_btn.addEventListener("click", (e) => {
      content_table.select_all();
    });
    const deselect_all_btn = document.getElementById("deselect-all-btn");
    deselect_all_btn.addEventListener("click", (e) => {
      content_table.deselect_all();
    });

    const order = {
      desc: true,
    };


    const e_sort_value = document.getElementById("sort-value");
    e_sort_value.addEventListener('change', async (e) => {
      order.column = e_sort_value.value;
      await content_editor.paginator.select_page(0);
    });

    const e_sort_desc = document.getElementById("sort-desc");
    e_sort_desc.addEventListener('click', async (e) => {
      if (order.desc) {
        order.desc = false;
        e_sort_desc.innerHTML = "&#xF103;";
      } else {
        order.desc = true;
        e_sort_desc.innerHTML = "&#xF104;";
      }
      await content_editor.paginator.select_page(0);
    });


    content_table_div.parentNode.removeChild(content_table_div);
    paginator.display.appendChild(content_table_div);
    await paginator.select_page(0);
  } catch (e) {
    console.error(e);
  }
});

