
import ContentItem from './item.mjs'
import CredParser from 'zunzun/authing.ui/cred/parser.mjs'
import Fingerprint from 'zunzun/flyfp.web/index.mjs'

const fp_base = 1.5;

export default class ContentTable {
  constructor(element) {
    this.element = element;
    this.name = element.getAttribute("name");

    const dev_fp = Fingerprint.get(fp_base);
    const fp_salt = Fingerprint.get_salt();
    this.cred_parser = new CredParser(dev_fp, fp_salt);

    for (let item of element.querySelectorAll("div.item")) {
      new ContentItem(item, this);
    }
  }

  get_column(name) {
    for (let column of content_model.columns) {
      if (column.name === name) {
        return column;
      }
    }
  }
}
