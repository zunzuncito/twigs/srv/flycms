
export default class ContentItem {
  constructor(element, content_table) {
    const _this = this;

    this.element = element;
    this.content_table = content_table;

    this.cred_parser = content_table.cred_parser;

    this.id = element.id;

    const options = element.querySelector("div.options");

    const edit_opt = options.querySelector("div.edit");
    edit_opt.addEventListener("click", async (e) => {
      try {
        _this.edit();
      } catch (e) {
        console.error(e);
      }
    });

    const remove_opt = options.querySelector("div.remove");
    remove_opt.addEventListener("click", async () => {
      try {
        _this.remove();
      } catch (e) {
        console.error(e);
      }
    });
  }

  static construct(id, obj, content_table) {
    const new_item_div = document.createElement("div");
    new_item_div.id = id;
    new_item_div.classList.add("tr", "item");

    const new_item_id = document.createElement("div");
    new_item_id.classList.add("td")
    if (id_type_uuid) {
      new_item_id.classList.add("uuid");
    } else {
      new_item_id.classList.add("int");
    }
    new_item_id.innerHTML = id;
    new_item_div.appendChild(new_item_id);

    for (let column of content_model.columns) {
      const new_val_div = document.createElement("div");
      new_val_div.classList.add("td", "val")

      if (column.type == "boolean") {
        new_val_div.classList.add("ck-box", "boolean");
        new_val_div.setAttribute("name", column.name);
        new_val_div.innerHTML = obj[column.name] ? "+" : "-"
      } else if (column.type == "text") {
        new_val_div.classList.add("text");
        new_val_div.setAttribute("name", column.name);
        new_val_div.real_value = obj[column.name];
        const tmp_div = document.createElement("div");
        tmp_div.innerHTML = obj[column.name];
        const dlimit = 200;
        let txt_suffix = "";
        if (tmp_div.innerText.length > dlimit) txt_suffix = "...";
        new_val_div.innerHTML = txt_suffix.length > 0 ? 
          tmp_div.innerText.slice(0, dlimit).trim()+txt_suffix :
          tmp_div.innerText;
      } else if (column.type == "media") {
        new_val_div.classList.add("media");
        new_val_div.setAttribute("name", column.name);
        new_val_div.innerHTML = obj[column.name];
      } else {
        new_val_div.classList.add(column.type);
        new_val_div.setAttribute("name", column.name);
        new_val_div.innerHTML = obj[column.name] || "";
      }
      new_item_div.appendChild(new_val_div);
    }


    const new_item_options = document.createElement("div");
    new_item_options.classList.add("td", "options")
    
    const new_item_edit = document.createElement("div");
    new_item_edit.classList.add("edit");
    new_item_edit.innerHTML = "&#xF10B;";
    new_item_options.appendChild(new_item_edit);

    const new_item_remove = document.createElement("div");
    new_item_remove.classList.add("remove");
    new_item_remove.innerHTML = "&#xF10C;";
    new_item_options.appendChild(new_item_remove);

    new_item_div.appendChild(new_item_options);
    
    return new ContentItem(new_item_div, content_table);
  }

  update(obj) {
    for (let column of content_model.columns) {
      if (typeof obj[column.name] !== "undefined") {
        const val_td = this.element.querySelector(`div.td[name="${column.name}"]`);
        if (column.type === "boolean") {
          val_td.innerHTML = obj[column.name] ? "+" : "-";
        } else if (column.type === "text") {
          val_td.innerHTML = obj[column.name]
          val_td.real_value = obj[column.name];
        } else {
          val_td.innerHTML = obj[column.name];
        }
      }
    }
  }

  async edit() {
    try {
      this.content_table.editor.edit(this);
    } catch (e) {
      console.error(e);
    }
  }

  async remove() {
    try {
      if (confirm(`Are you sure you want to remove this row?`)) {
        display_loading_overlay();
        const creds = await this.cred_parser.next();
        await fetch(`/flycms-admin-api/${this.content_table.name}/${this.id}`, {
          method: "DELETE",
          headers: {
            "Authorization": `cks=${creds.cks}&authorized=${creds.authorized}`
          }
        });

        total_item_count--;

        if (total_item_count >= page_size) {
          const creds = await this.cred_parser.next({
            offset: current_page_index*page_size + page_size-1,
            limit: 1
          });
          const model_page = await (await fetch(`/flycms-admin-api/${content_model.api_name}?cks=${creds.cks}&authorized=${creds.authorized}`)).json();
          for (const pmodel of model_page) {
            const mid = pmodel.id;
            delete pmodel.id;
            const new_item = ContentItem.construct(mid, pmodel, this.content_table);
            this.content_table.element.appendChild(new_item.element);
          }
        }

        this.content_table.paginator.resize(Math.ceil(total_item_count/page_size));
        this.content_table.element.removeChild(this.element);

        hide_loading_overlay();
      }
    } catch (e) {
      console.error(e);
    }
  }

  get_values() {
    const values = [];

    for (let column of content_model.columns) {
      const val_td = this.element.querySelector(`div.td[name="${column.name}"]`);
      if (column.type == "boolean") {
        values[column.name] = val_td.innerHTML === "+" ? true : false;
      } else if (column.type == "text") {
        values[column.name] = val_td.real_value
      } else {
        values[column.name] = val_td.innerHTML;
      }
    }
    return values;
  }
}
