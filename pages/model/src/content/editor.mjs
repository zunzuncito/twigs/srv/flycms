
import ContentItem from './item.mjs'

import NumInput from "zunzun/flyhtml/input/number.mjs"

export default class ContentEditor {
  constructor(element, content_table, cred_parser ) {
    const _this = this;
    this.element = element;
    this.content_table = content_table;


    this.cred_parser = cred_parser;

    for (let einput of this.element.querySelectorAll(".editor-input")) {
      if (einput.type == "number") {
        new NumInput(einput)
      }
    }

    if (content_model.lang) {
      const ilang_select = element.querySelector("div.ilang-select select");
      this.cilang = ilang_select.value;
      ilang_select.addEventListener("change", (e) => {
        _this.cilang = ilang_select.value;

        for (let einput of _this.element.querySelectorAll(".editor-input")) {

          let input_lang = einput.getAttribute("lang");
          if (input_lang) {
            if (einput.classList.contains("rich-text-input")) {
              if (input_lang !== this.cilang) {
                einput.parentElement.style.display = "none";
              } else {
                einput.parentElement.style.display = "";
              }
            } else {
              if (input_lang !== this.cilang) {
                einput.style.display = "none";
              } else {
                einput.style.display = "";
              }
            }
          }
        }

      });
    }


    const editor_close = element.querySelector("div.close-editor");
    editor_close.addEventListener("click", (e) => {
      document.body.removeChild(element);
    });


    const editor_submit = element.querySelector("input[type=submit]");
    editor_submit.addEventListener('click', async (e) => {
      try {
        display_loading_overlay();
        await _this.submit();
        hide_loading_overlay();
      } catch (err) {
        console.error(err);
      }
    });

    document.body.removeChild(element);
  }

  edit(item) {
    this.current_item = item;

    let values = undefined;
    if (item) values = item.values;

    for (let einput of this.element.querySelectorAll(".editor-input")) {

      let default_value = einput.getAttribute("default") || "";
          console.log("default_value", default_value);
      let ikey = einput.getAttribute("name");
      if (einput.type == "checkbox") {
        einput.checked = values ? values[ikey] : false;
      } else if (einput.classList.contains("rich-text-input")) {
        let input_lang = einput.getAttribute("lang");
        if (input_lang) {
          if (input_lang !== this.cilang) {
            einput.parentElement.style.display = "none";
          } else {
            einput.parentElement.style.display = "";
          }

          if (values && typeof values[ikey] != "object") values[ikey] = JSON.parse(values[ikey]);
          einput.innerHTML = values ? (
            values[ikey] ? values[ikey][input_lang] : ""
          ) : "";
        } else {
          einput.innerHTML = values ? values[ikey] : "";
        }
      } else if (this.content_table.get_column(ikey).hash) {
        einput.value = "";
      } else {
        let input_lang = einput.getAttribute("lang");
        if (input_lang) {
          if (input_lang !== this.cilang) {
            einput.style.display = "none";
          } else {
            einput.style.display = "";
          }

          console.log(values, ikey);
          if (values && typeof values[ikey] != "object") values[ikey] = JSON.parse(values[ikey]);
          einput.value = values ? (
            values[ikey] ? values[ikey][input_lang] : ""
          ) : "";
        } else {
          einput.value =  values ? values[ikey] : default_value;
          console.log("einput",einput);
        }
      }
    }

    document.body.appendChild(this.element);
  }

  async submit() {
    try {
      console.log("current_item", this.current_item);
      let req_url = undefined;
      let req_method = undefined;

      if (this.current_item) {
        req_url = `/flycms-admin-api/${this.content_table.name}/${this.current_item.values.id}`
        req_method = "PUT";
      } else {
        req_url = `/flycms-admin-api/${this.content_table.name}`;
        req_method = "POST";
      }

      const formobj = {};
      for (let einput of this.element.querySelectorAll(".editor-input")) {
        let ikey = einput.name || einput.getAttribute("name");
        if (einput.type == "checkbox") {
          formobj[ikey] = einput.checked;
        } else if (einput.classList.contains("rich-text-input")) {
          let input_lang = einput.getAttribute("lang");
          if (input_lang) {
            if (typeof formobj[ikey] != "object") formobj[ikey] = {};
            formobj[ikey][input_lang] = einput.innerHTML;
          } else {
            formobj[ikey] = einput.innerHTML;
          }
        } else {
          let input_lang = einput.getAttribute("lang");
          if (input_lang) {
            if (typeof formobj[ikey] != "object") formobj[ikey] = {};
            formobj[ikey][input_lang] = einput.value;
          } else {
            if (einput.type == "number") {
              formobj[ikey] = parseInt(einput.value);
            } else {
              formobj[ikey] = einput.value;
            }
          }
        }
      }

      for (let key in content_model.columns) {
        const column = content_model.columns[key];
        if (column.type == "text" && column.lang) {
          formobj[key] = JSON.stringify(formobj[key]);
        } else if (column.type == "text[]") {
          formobj[key] = formobj[key] ? formobj[key].split(", ") : [];
        }
      }

      console.log(formobj);

      const creds = await this.cred_parser.next(formobj);
      const resp_obj_enc = await (await fetch(req_url, {
        method: req_method,
        body: `cks=${creds.cks}&authorized=${creds.authorized}` // formData should contain the file
      })).text();

      console.log("cred_parser", this.cred_parser);
      console.log("iaes", this.cred_parser.creds.iaes);
      console.log("decrypt", resp_obj_enc);
      const resp_obj = JSON.parse(this.cred_parser.creds.iaes.decrypt(resp_obj_enc));
      console.log("resp_obj", resp_obj);

      const new_id = resp_obj.id;
      delete resp_obj.id;

      for (let key in resp_obj) {
        formobj[key] = resp_obj[key];
      }

      if (this.current_item) {
        this.current_item.update(formobj);
      } else {
        total_item_count++;
        console.log("total_item_count", total_item_count);
        console.log("resize", Math.ceil(total_item_count/page_size));

        await this.paginator.resize(Math.ceil(total_item_count/page_size));
        await this.paginator.select_page(this.paginator.cur_page.index);
      }

      document.body.removeChild(this.element); 
    } catch (e) {
      console.error(e);
    }
  }

}
