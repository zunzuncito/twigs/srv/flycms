
import fs from "fs"
import path from "path"

import {fileTypeFromStream} from 'file-type'

export default class Middleware {

  static async render(req, res, njk, ctx, sm) {
    try {
      const flycms = sm.get_service_instance("flycms");

      const name = req.url.match(/^\/flycms\/model\/([^?\/]+)(\/|\/index\.html)?/)[1];

      const model = flycms.models[name];

      ctx.title = model.name;
      ctx.model_name = name;
      ctx.model = {
        columns: model.columns,
        uuid: model.uuid,
        name: model.name,
        api_name: model.api_name,
        lang: model.lang
      }

      console.log("COLUMNS", model.columns);

      for (let c = 0; c < ctx.model.columns.length; c++) {
        if (typeof ctx.model.columns[c].references == "string") {
          const model_name = ctx.model.columns[c].references.slice(13);
          const model_inst = flycms.models[model_name];
          if (model_inst) {
            ctx.model.columns[c].type = "select";
            const select_columns = ['id'];
            const option_text = ctx.model.columns[c].option_text;
            if (option_text) select_columns.push(option_text);
            const options = await model_inst.select(select_columns);
            for (let o = 0; o < options.length; o++) {
              options[o] = {
                value: options[o].id,
                text: option_text ? options[o][option_text] : options[o].id
              }
            }
            ctx.model.columns[c].options = options;
          }
        }
      }

      const total_items = parseInt(await flycms.models[name].count());
      ctx.page_size = 12;
      ctx.page_count = Math.ceil(total_items/ctx.page_size);
      ctx.total_item_count = total_items;
      if (typeof ctx.total_item_count != "number") ctx.total_item_count = 0;


      console.log("ctx.total_item_count", ctx.total_item_count);
      if (flycms.albums[name]) {
        const media_total_items = await flycms.albums[name].count();
        ctx.media_page_size = 12;
        ctx.media_page_count = Math.ceil(media_total_items/ctx.media_page_size);
      }

      if (!model.allowed(req)) {
        res.writeHead(401, "Unauthorized", {"Content-Type": "text/plain"});
        res.end("Unauthorized");
        return;
      }



      if (flycms.albums[name]) {
        ctx.album_name = name;
        ctx.media = await flycms.albums[name].get_page(0, ctx.media_page_size);
//        ctx.media.items = await flycms.albums[name].get_page(0, ctx.media_page_size);
      }

/*      ctx.items = await model.select("*", {
        offset: 0,
        limit: ctx.page_size
      });*/


      return njk;
    } catch (e) {
      console.error(e.stack);
      return njk;
    }
  }

}
